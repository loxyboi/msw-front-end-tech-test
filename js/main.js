var CarouselView = (function() {

	return Backbone.View.extend({

		currentPane: 0,

		events: {
			'click .left': 'left',
			'click .right': 'right'
		},

		initialize: function (options) {
			this.$inner = this.$el.find('.carousel-inner');

			this.$panes = this.$inner.find('.carousel-pane');

			this.numPanes = this.$panes.length;
		},

		left: function () {
			if (this.currentPane !== 0) {
				this.currentPane--;

				this.showPane(this.currentPane, true);
			}
		},

		right: function () {
			if (this.currentPane !== this.$panes.length - 1) {
				this.currentPane++;

				this.showPane(this.currentPane, true);
			}
		},

		showPane: function (pane, animate) {
			var offset = (100 / this.numPanes) * pane;

			this.$inner.removeClass('animate');

			if (animate) {
				this.$inner.addClass('animate');
			}

			this.$inner.css('transform', 'translateX(-'+offset+'%)');
		}

	});

})();